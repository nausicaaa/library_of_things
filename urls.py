from django.conf import settings
from django.conf.urls import include, patterns, url
from django.conf.urls.static import static
from django.contrib import admin
from notifications import urls as notifications_urls

from apps.lot.views import (
    ArticleListView,
    SelfProfileDeleteView,
    SelfProfileUpdateView,
    SelfProfileView,
    UserProfileView,
    search_article,
)

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', ArticleListView.as_view(), name='main'),
    url(r'^$', search_article, name='search'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^messages/', include('django_messages.urls')),
    url(r'^lot/', include('apps.lot.urls')),
    url(r'^accounts/profile/?$', SelfProfileView.as_view(), name='self-profile'),
    url(r'^accounts/profile/(?P<slug>[\w\-]+)/update/?$', SelfProfileUpdateView.as_view(), name='update'),
    url(r'^accounts/profile/(?P<slug>[\w\-]+)/delete/?$', SelfProfileDeleteView.as_view(), name='delete-user'),
    url(r'^accounts/profile/(?P<slug>[\w\-]+)/', UserProfileView.as_view(), name='user-profile'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
