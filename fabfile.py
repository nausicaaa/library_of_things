from __future__ import with_statement

from fabric.api import cd, env, run

env.use_ssh_config = True
env.hosts = ['lot']


def deploy():
    code_dir = '~/library_of_things'
    with cd(code_dir):
        run("git pull")
