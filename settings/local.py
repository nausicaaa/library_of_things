from .base import *  # noqa

ADMINS = (
    ('Aneta', 'anetka.wlodarczyk@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'library_of_things_test.db',
    }
}

MY_APPS = (
    'django_extensions',
    'debug_toolbar',
)

INSTALLED_APPS = INSTALLED_APPS + MY_APPS

# You might want to use sqlite3 for testing in local as it's much faster.
# stupid
# if len(sys.argv) > 1 and 'test' in sys.argv[1]:
#     DATABASES = {
#         'default': {
#             'ENGINE': 'django.db.backends.sqlite3',
#             'NAME': '/tmp/library_of_things_test.db',
#         }
#     }

SHELL_PLUS_POST_IMPORTS = (
    ('apps.lot.factories', ('ArticleFactory', 'UserFactory', 'TagFactory')),
)

LOGIN_REDIRECT_URL = '/'
