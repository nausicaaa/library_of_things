from django.contrib import admin

from apps.lot.models import Article, Tag, User

admin.site.register(User)
admin.site.register(Article)
admin.site.register(Tag)
