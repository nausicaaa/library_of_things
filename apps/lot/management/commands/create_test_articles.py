from django.core.management.base import BaseCommand

from apps.lot.factories import ArticleFactory
from apps.lot.models import Tag, User


class Command(BaseCommand):
    help = "My shiny new management command."

    def add_arguments(self, parser):
        parser.add_argument('articles_no', type=int)

    def handle(self, *args, **options):
        test_tag, _ = Tag.objects.get_or_create(name='test')
        test_user, _ = User.objects.get_or_create(username='tester')
        articles_no = options['articles_no']
        for _ in range(articles_no):
            ArticleFactory(belongs_to=test_user, tags=[test_tag])
