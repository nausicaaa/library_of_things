from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name


class Article(models.Model):
    belongs_to = models.ForeignKey('User', related_name='owned_articles')
    borrowed_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='borrowed_articles',
        null=True,
        on_delete=models.SET_NULL
    )
    last_borrowed_by = models.ForeignKey('User',null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=100)
    date_of_return = models.DateTimeField(null=True, blank=True, verbose_name='When article should be given back?')
    photo = models.ImageField(upload_to='article_photos', null=True, blank=True)
    tags = models.ManyToManyField(Tag, verbose_name='Category of your article:', blank=True)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('article-detail', kwargs={'pk': self.pk})


class User(AbstractUser):
    city = models.CharField(max_length=100)
    avatar = models.ImageField(upload_to='avatars', null=True)

    def __unicode__(self):
        return self.username

# shouldn't get_absolute_url be here?

class Group(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name
