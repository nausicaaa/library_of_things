from django.conf.urls import patterns, url

from apps.lot.views import (
    ArticleCreateView,
    ArticleDeleteView,
    ArticleDetailView,
    ArticleUpdateView,
    borrow_article,
    search_article,
    show_borrowed_articles,
    show_lent_articles,
    show_owned_articles,
    not_borrowed, wish_of_article_return, confirm_article_return)

urlpatterns = patterns(
    '',
    url(r'^add$', ArticleCreateView.as_view(), name='add_article'),
    url(r'^article/(?P<pk>[0-9]+)/', ArticleDetailView.as_view(), name='article-detail'),
    url(r'^update/article/(?P<pk>[0-9]+)/', ArticleUpdateView.as_view(), name='article-update'),
    url(r'^article/search', search_article, name='search'),
    url(r'^article/borrow/(?P<pk>[0-9]+)/', borrow_article, name='article-borrow'),
    url(r'^article/return/(?P<pk>[0-9]+)/', wish_of_article_return, name='wish-of-return'),
    url(r'^article/not_borrowed/(?P<pk>[0-9]+)/', not_borrowed, name='not-borrowed'),
    url(r'^article/confirm_return/(?P<pk>[0-9]+)/', confirm_article_return, name='confirm-return'),
    url(r'^article/myarticles/', show_owned_articles, name='my-articles'),
    url(r'^article/borrowedarticles/', show_borrowed_articles, name='borrowed-articles'),
    url(r'^article/lent/', show_lent_articles, name='lent-articles'),
    url(r'^article/delete/(?P<pk>[0-9]+)/', ArticleDeleteView.as_view(), name='article-delete'),
)

# lambda request : HttpResponse('pusty')
