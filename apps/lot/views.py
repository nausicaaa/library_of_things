# coding=utf-8
from braces.views import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.views.generic import CreateView, DeleteView, DetailView, ListView, TemplateView, UpdateView
from django_messages.models import Message

from apps.lot.models import Article, User


@login_required
def borrow_article(request, pk):
    article = Article.objects.get(pk=pk)
    if article.belongs_to == request.user:
        raise Http404
    article.borrowed_by = request.user
    context = {'article': article, 'sender': request.user.username, 'recipient': article.belongs_to}
    Message(
        subject='%s borrowed %s' % (request.user.username, article.name),
        body=mark_safe(render_to_string('borrowed_message_body.html', context)),
        sender=request.user,
        recipient=article.belongs_to
    ).save()
    article.save()
    return redirect('main')

# powinna byc inna wiadomosc w zaleznosci czy przedmiot wypozyczony i nie chce wypozyczyc czy oddany
@login_required
def not_borrowed(request, pk):
    article = Article.objects.get(pk=pk)
    borrower = article.borrowed_by
    article.borrowed_by = None
    Message(
        subject='%s doesn\'t want to lend you %s' % (request.user.username, article.name),
        sender=request.user,
        recipient=borrower
    ).save()
    article.save()
    return redirect('article-detail', pk=article.pk)

@login_required
def confirm_article_return(request,pk):
    article = Article.objects.get(pk=pk)
    article.borrowed_by = None
    last_borrower = article.last_borrowed_by
    Message(
        subject='%s confirmed return of %s' % (request.user.username, article.name),
        sender=request.user,
        recipient=last_borrower
    ).save()
    article.save()
    return redirect('article-detail', pk=article.pk)


@login_required
def wish_of_article_return(request, pk):
    article = Article.objects.get(pk=pk)
    article.last_borrowed_by = article.borrowed_by
    context = {'article': article, 'sender': request.user.username, 'recipient': article.belongs_to}
    Message(
        subject='%s gave back %s' % (request.user.username, article.name),
        body=mark_safe(render_to_string('lot/returned_message_body.html', context)),
        sender=request.user,
        recipient=article.belongs_to
    ).save()
    article.save()
    return redirect('article-detail', pk=article.pk)


@login_required
def search_article(request):
    search_query = request.GET.get('search_box')
    results = Article.objects.filter(name__icontains=search_query)
    return render(request, template_name='lot/article_list.html', context={'object_list': results})


@login_required
def show_owned_articles(request):
    owned_articles = Article.objects.filter(belongs_to=request.user)
    return render(request, template_name='lot/article_list.html', context={'object_list': owned_articles})


@login_required
def show_borrowed_articles(request):
    borrowed_articles = Article.objects.filter(borrowed_by=request.user)
    return render(request, template_name='lot/article_list.html', context={'object_list': borrowed_articles})


@login_required
def show_lent_articles(request):
    lent_articles = Article.objects.filter(belongs_to=request.user, borrowed_by__isnull=False).all()
    return render(request, template_name='lot/article_list.html', context={'object_list': lent_articles})


class ArticleListView(ListView):
    model = Article


class ArticleDetailView(DetailView):
    model = Article


class ArticleCreateView(LoginRequiredMixin, CreateView):
    model = Article
    fields = ['name', 'photo', 'date_of_return', 'tags']
    template_name_suffix = '_create_form'

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        form.instance.belongs_to = request.user
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class ArticleUpdateView(LoginRequiredMixin, UpdateView):
    model = Article
    fields = ['name', 'photo', 'date_of_return', 'tags']
    template_name_suffix = '_update_form'

    def get_success_url(self):
        article = get_object_or_404(self.model, pk=self.kwargs['pk'])
        return article.get_absolute_url()


class ArticleDeleteView(LoginRequiredMixin, DeleteView):
    model = Article

    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user. """
        article = super(ArticleDeleteView, self).get_object(queryset=queryset)
        if article.belongs_to != self.request.user:
            raise Http404
        return article

    def get_success_url(self):
        return reverse('main')


class UserProfileView(LoginRequiredMixin, DetailView):
    model = User
    slug_field = 'username'

    def dispatch(self, request, *args, **kwargs):
        if kwargs['slug'] == request.user.username:
            return redirect('self-profile')
        return super(UserProfileView, self).dispatch(request, *args, **kwargs)


class SelfProfileView(LoginRequiredMixin, TemplateView):
    model = User
    template_name = 'lot/self_profile.html'


class SelfProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    slug_field = 'username'
    fields = ['city', 'avatar']
    template_name_suffix = '_update_form'
    success_url = 'changed'


class SelfProfileDeleteView(LoginRequiredMixin, DeleteView):
    model = User
    slug_field = 'username'
    success_url = 'changed'
