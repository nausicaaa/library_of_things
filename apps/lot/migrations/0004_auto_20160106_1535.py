# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lot', '0003_auto_20160106_1437'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='date_of_return',
            field=models.DateTimeField(null=True),
        ),
    ]
