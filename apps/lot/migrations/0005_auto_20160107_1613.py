# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lot', '0004_auto_20160106_1535'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='avatar',
            field=models.ImageField(null=True, upload_to=b'avatars'),
        ),
        migrations.AlterField(
            model_name='article',
            name='photo',
            field=models.ImageField(upload_to=b'article_photos'),
        ),
    ]
