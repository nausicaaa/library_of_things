# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lot', '0011_auto_20160219_1356'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='date_of_return',
            field=models.DateTimeField(null=True, verbose_name=b'When article should be given back?', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='tags',
            field=models.ManyToManyField(to='lot.Tag', verbose_name=b'Category of your article:'),
        ),
    ]
