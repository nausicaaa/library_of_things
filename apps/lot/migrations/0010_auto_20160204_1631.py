# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lot', '0009_auto_20160121_1245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='borrowed_by',
            field=models.ForeignKey(related_name='borrowed_articles', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
