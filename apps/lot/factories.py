import factory
from factory import DjangoModelFactory
from factory.declarations import Sequence, SubFactory

from apps.lot.models import Article, Tag, User


class TagFactory(DjangoModelFactory):
    class Meta:
        model = Tag

    name = 'test'


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    username = Sequence(lambda n: 'user_%d' % n)


class ArticleFactory(DjangoModelFactory):
    class Meta:
        model = Article

    name = Sequence(lambda n: 'Article %d' % n)
    belongs_to = SubFactory(UserFactory)
    # borrowed_by = SubFactory(UserFactory)
    # date_of_return = datetime.datetime.today()
    # photo

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for tag in extracted:
                self.tags.add(tag)
